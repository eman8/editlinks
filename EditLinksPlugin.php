<?php

/*
 * E-man Plugin
 *
 * Functions to customize Omeka for the E-man Project
 *
 */

class EditLinksPlugin extends Omeka_Plugin_AbstractPlugin
{

  protected $_filters = array(
   		'public_navigation_admin_bar',
  );

  public function filterPublicNavigationAdminBar($nav)
  {
    	if ($currentUser = current_user()) {
  			$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
  	  	$editLink = $editPart = "";
  	  	if (isset($params['controller'])) {
   	      $nomContenu = ' ce contenu';
  	  		if (in_array($params['controller'], array('page', 'items', 'collections', 'files', 'eman', 'exhibits')) && $params['action'] <> 'browse') {
  	  			if (in_array($currentUser->role, array('super', 'admin', 'editor'))) {
    	  			if (isset($params['id'])) {
                $editPart = '/edit/'. $params['id'];
      	  	  }
  	  				if ($params['controller'] == 'eman') {
  	  					$controller = explode("-", $params['action']);
  	  					$controller = $controller[0];
                $editPart = '/edit/'. $params['id'];
   	  				} else {
  	  					$controller = $params['controller'];
  	  				}
  	  				if(isset($params['module'])) {
  	  				  // Transcript
  	  				  if ($params['module'] == 'transcript' && isset($params['term'])) {
      	  				$controller = 'transcript/editterm';
      	  				$db = get_db();
      	  				$id = $db->query("SELECT id FROM `$db->TranscriptTerms` WHERE name = " . $db->quote($params['term']))->fetchObject();
      	  				$editPart = '?termId=' . $id->id;
                  $nomContenu = ' ce terme';
    	  				}
  	  				  // Simple Pages
  	  				  if ($params['module'] == 'simple-pages') {
      	  				$controller = 'simple-pages/index';
      	  				$editPart = '/edit/id/' . $params['id'];
                  $nomContenu = ' cette Simple Page';
    	  				}
  		  				// Exhibit Builder
    	  				if ($params['module'] == 'exhibit-builder') {
      	  				$controller = 'exhibits';
      	  				$db = get_db();
      	  				if ($params['action'] <> 'summary') {
                    $page = $db->query("SELECT id FROM `$db->ExhibitPage` WHERE slug = '" . $params['page_slug_1'] . "'")->fetchAll();
      	  					$editPart = '/edit-page/' . $page[0]['id'];
      	  					$nomContenu = ' cette page d\'exposition';
                  } else {
                  	$front = $db->query("SELECT id FROM `$db->Exhibits` WHERE slug = '" . $params['slug'] . "'")->fetchAll();
                  	$editLink = '/exhibits/edit/' . $front[0]['id'];
                  	$nomContenu = ' cette exposition';
                  	$editPart = '';
                  }

    	  				}
  	  				}
  	  				if ($controller && $editPart) {
      					$editLink = $controller . $editPart;
  	  				}
  	  			}
  	  		}
  	  	} else {
    		  return $nav;
        }
    	}
    	if (isset($editLink)) {
      	$nav[] = array(
          'label' => __('Editer ' . $nomContenu),
          'uri' => url('/admin/' . $editLink)
        );
    	}
      return $nav;
  }
}