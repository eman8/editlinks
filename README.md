This very simple module adds "Edit this content" type links to the admin bar for authorized users.

It works with Simple Pages and Exhibits as well as items, collections and files.

Ce module très simple ajoute des liens de type “Editer ce contenu” à la barre d'administration pour les utilisateurs autorisés. Il fonctionne avec des "simples pages", des pages d'expositions ainsi qu'avec des items, des collections et des fichiers.

## Credits

Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen). Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/pluginseman#A2)

